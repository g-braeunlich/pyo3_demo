# Quickstart

* Clone
* Build the rust extension (omit `--release` for an unoptimized debug
  build):
  ```bash
  cargo build --release
  ```
  Then move the resulting library in `target/release` to folder
   `example` containing the python script and name it `pyo3_mm.so`
  (`pyo3_mm.pyd` on windows).
  
  For linux only, there is a script doing build and move
  automatically:
  
  ```bash
  DEST=example/ ./build-inplace.sh --release
  ```

* Call the extension from python:
  ```bash
  ./example.py
  ```
* Optional: build the C extension (assuming you are again in the repos
  toplevel dir):
  ```bash
  cd c_mm
  ./setup.py build_ext --inplace
  cd -
  ln -s c_mm/c_mm.cpython-* c_mm.so
  ```
  Then replace rerun `example.py`.
