#!/usr/bin/env python3
"""
To only compile the C extension inplace:
python setup.py build_ext --inplace
"""

try:
    from setuptools import setup, find_packages, Extension
except ImportError:
    raise RuntimeError('setuptools is required')
try:
    from numpy import get_include as _numpy_get_include
except ImportError:
    raise RuntimeError('numpy is required')

setup(name="c_mm",
      version="1.0",
      packages=find_packages(),
      package_dir={"c_mm": "."},
      description="C matrix multiplication",
      author='Gerhard Bräunlich',
      author_email='g.braeunlich@disroot.org',
      keywords=[],
      install_requires=["numpy"],
      setup_requires=["numpy"],
      classifiers=[],
      python_requires=">=3.5",
      ext_package="c_mm",
      ext_modules=[Extension('c_mm',
                             sources=["c_mm.c"],
                             include_dirs=[_numpy_get_include()])]
      )
