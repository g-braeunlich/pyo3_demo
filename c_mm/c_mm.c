#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

static _Bool check(_Bool result, PyObject *err_type, const char* err_msg, ...);
static _Bool check_matrix_match(PyArrayObject *A, PyArrayObject *B);

static PyObject *py_matmul(PyObject *self, PyObject *args, PyObject *keywords)
{
  unsigned int i, j, k;
  double x;
  PyArrayObject *py_A, *py_B;
  PyObject *py_out = NULL;

  if(PyArg_ParseTupleAndKeywords(args, keywords,
				 "O!O!:c_calc_P",
				 (char*[]){
				   "A", "B", NULL},
				 &PyArray_Type, &py_A,
				 &PyArray_Type, &py_B)
     // Validate arguments
     && check(PyArray_NDIM(py_A) == 2, PyExc_ValueError, "A must be 2-dimensional")
     && check(PyArray_NDIM(py_B) == 2, PyExc_ValueError, "B must be 2-dimensional")
     && check_matrix_match(py_A, py_B)
     )
    {
      npy_long n = PyArray_DIMS(py_A)[0];
      npy_long l = PyArray_DIMS(py_A)[1];
      npy_long m = PyArray_DIMS(py_B)[1];
#define C_NDIM(arr) sizeof(arr)/sizeof(npy_intp)
      npy_intp dims_out[] = {n, m};
      py_out = PyArray_SimpleNew(C_NDIM(dims_out), dims_out, NPY_DOUBLE);
#undef C_NDIM
      double *a = (double*)PyArray_DATA((PyArrayObject*)py_A);
      double *b = (double*)PyArray_DATA((PyArrayObject*)py_B);
      double *c = (double*)PyArray_DATA((PyArrayObject*)py_out);
      for(i=0; i<n; i++)
	for(j=0; j<m; j++)
	  {
	    x = 0.;
	    for(k=0; k<l; k++) x += a[i*l + k] * b[k*m + j];
	    c[i*m + j] = x;
	  }
      Py_IncRef(py_out);
    }
  return py_out;
}

static _Bool check(_Bool result, PyObject *err_type, const char* err_msg, ...)
{
  if(!result)
    {
      va_list ap;
      va_start(ap, err_msg);
      PyErr_FormatV(err_type, err_msg, ap);
      va_end(ap);
    }
  return result;
}

static _Bool check_matrix_match(PyArrayObject *A, PyArrayObject *B)
{
  return check(PyArray_DIMS(A)[1] == PyArray_DIMS(B)[0], PyExc_ValueError,
	       "Dimension mismatch: A is %dx%d, B is %dx%d", PyArray_DIMS(A)[0], PyArray_DIMS(A)[1], PyArray_DIMS(B)[0], PyArray_DIMS(B)[1]);
}


static PyMethodDef methods[] = {
  { "matrix_multiplication", (PyCFunction)py_matmul, METH_VARARGS | METH_KEYWORDS, "matrix_multiplication" },
  { NULL, NULL, 0, NULL }
};
static struct PyModuleDef module_def = {
  .m_base = PyModuleDef_HEAD_INIT,
  .m_name = "c_mm",   /* name of module */
  .m_doc = NULL, /* module documentation, may be NULL */
  .m_size = -1,       /* size of per-interpreter state of the module,
	       or -1 if the module keeps state in global variables. */
  .m_methods = methods,
  .m_slots = NULL
};
PyMODINIT_FUNC
PyInit_c_mm(void)
{
  import_array();
  return PyModule_Create(&module_def);
}
