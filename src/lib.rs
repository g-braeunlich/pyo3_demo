#![allow(non_snake_case)]

use numpy::{PyArray2, PyReadonlyArray2};
use pyo3::exceptions::PyValueError;
use pyo3::prelude::{pymodule, Py, PyModule, PyResult, Python};

#[pymodule]
fn pyo3_mm(_py: Python, m: &PyModule) -> PyResult<()> {
    #[pyfn(m, "matrix_multiplication")]
    fn matrix_multiplication(
        py: Python,
        A: PyReadonlyArray2<f64>,
        B: PyReadonlyArray2<f64>,
    ) -> PyResult<Py<PyArray2<f64>>> {
        let (n, l, m) = (A.dims()[0], A.dims()[1], B.dims()[1]);
        if l != B.dims()[0] {
            Err(PyValueError::new_err(format!(
                "Dimension mismatch: A is {}x{}, B is {}x{}",
                n,
                l,
                B.dims()[0],
                m
            )))?;
        }
        let C = PyArray2::<f64>::new(py, [n, m], false);
        mm(
            A.as_slice()?,
            B.as_slice()?,
            unsafe { C.as_slice_mut()? },
            n,
            m,
            l,
        );
        Ok(C.to_owned())
    }

    Ok(())
}

fn mm(A: &[f64], B: &[f64], C: &mut [f64], n: usize, m: usize, l: usize) {
    let mut x;
    for i in 0..n {
        for j in 0..m {
            x = 0.;
            for k in 0..l {
                x += A[i * l + k] * B[k * m + j];
            }
            C[i * m + j] = x;
        }
    }
}
