#!/bin/bash

DEST="${DEST:-./}"

target="debug"
[[ "$1" == "--release" ]] && target="release"

project=$(grep -Po '^name ?= ?"\K[^"]*' Cargo.toml | tail -n1)

cargo build $@
cp target/${target}/lib${project}.so "${DEST}/${project}.so"
