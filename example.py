#!/usr/bin/env python3

import numpy
from pyo3_mm import matrix_multiplication
try:
    from c_mm import matrix_multiplication as c_matrix_multiplication
    C_EXT = True
except ImportError:
    C_EXT = False

print("[Test 1]")

A = numpy.array([[1., 2.], [3., 0.]])
B = numpy.array([[0., 1.], [1., 0.]])
C = matrix_multiplication(A, B)

print("🦀 PyO3\n", C)
print("🐍 numpy\n", A @ B)
if C_EXT:
    print("C\n", c_matrix_multiplication(A, B))

print()
print("[Test 2]")

A = numpy.array([[1., 2.], [3., 0.]])
B = numpy.array([[0., 1., 0.], [1., 0., 0.]])
C = matrix_multiplication(A, B)

print("🦀 PyO3\n", C)
print("🐍 numpy\n", A @ B)
if C_EXT:
    print("C\n", c_matrix_multiplication(A, B))

print()
print("Lets see what happens if we multiply matrices of non matching shapes:")
A = numpy.array([[1., 2.], [3., 0.]])
B = numpy.array([[0., 1.], [1., 0.], [0., 0]])
try:
    matrix_multiplication(A, B)
except BaseException as e:
    print("🦀 PyO3 -> 💥", repr(e))
if C_EXT:
    try:
        c_matrix_multiplication(A, B)
    except BaseException as e:
        print("C -> 💥", repr(e))

print()
print("Lets see what happens if one of the operands is not a matrix")
A = numpy.array([3., 0.])
B = numpy.array([[0., 1.], [1., 0.], [0., 0]])
try:
    matrix_multiplication(A, B)
except BaseException as e:
    print("🦀 PyO3 -> 💥", repr(e))
if C_EXT:
    try:
        c_matrix_multiplication(A, B)
    except BaseException as e:
        print("C -> 💥", repr(e))
